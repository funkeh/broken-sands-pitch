function love.load()

    -- Library shorthands
    a = love.audio
    g = love.graphics

    -- Load graphical resources
    gfx = {
        bg = g.newImage("gfx/bg.png"),
        ch = g.newImage("gfx/chikun.png"),
        cl = g.newImage("gfx/clock.png"),
        co = g.newImage("gfx/clockhand.png"),
        de = g.newImage("gfx/dev.png"),
        dg = g.newImage("gfx/dodge.png"),
        dl = g.newImage("gfx/dollas.png"),
        en = g.newImage("gfx/enemy.png"),
        gr = g.newImage("gfx/got.png"),
        gg = g.newImage("gfx/got-glow.png"),
        go = g.newImage("gfx/goal.png"),
        gt = g.newImage("gfx/grain.png"),
        hi = g.newImage("gfx/hi.png"),
        le = g.newImage("gfx/level.png"),
        lo = g.newImage("gfx/logo.png"),
        lp = g.newImage("gfx/levelPlan.png"),
        me = g.newImage("gfx/members.png"),
        pl = g.newImage("gfx/player.png"),
        un = g.newImage("gfx/uni.png"),
        we = g.newImage("gfx/weapons.png")
    }

    -- Music
    bgm = {
        ns = a.newSource("snd/newShips.mp3"),
        vo = a.newSource("snd/kickstarterAudioMore.mp3")
    }

    bgm.ns:setVolume(0.85)

    bg = false      -- Show bg?
    timer = 0       -- How much time has passed
    timerStep = 0   -- How fast time passes

    spin = 0        -- Clock spinning

    g.setFont(g.newFont(24))

end


function love.update(dt)

    -- Increase timer
    timer = timer + timerStep * dt

    if timer > 14 then

        if timer < 14.8 then

            spin = spin + timerStep * dt

        else

            spin = spin + timerStep * dt * 0.15

        end

    end


end


function love.keypressed(key)

    -- If key is spacebar
    if key == " " then

        -- Start timer counting
        timerStep = 1

        --bg = true
        timer = 0
        bgm.ns:play()
        bgm.ns:seek(timer, "seconds")
        bgm.vo:play()
        bgm.vo:seek(timer, "seconds")

    end

end


function love.draw()

    g.setColor(255, 255, 255)

    if bg then
        g.draw(gfx.bg, 0, 0)
    end

    -- Draw things if at time
    if timer < 0.8 then

        -- "Hi"
        g.draw(gfx.hi, 0, 0)

    elseif timer < 1.7 then

        g.draw(gfx.ch, 0, 0)

    elseif timer < 2.5 then

        g.draw(gfx.me, 0, 0)

    elseif timer < 9.0 then

        -- Don't do shit

    elseif timer < 10.5 then

        g.draw(gfx.lo, 0, 0)

    elseif timer < 12.0 then

        -- Tag bg on
        bg = true

    elseif timer < 13.0 then

        -- Person

    elseif timer < 14.0 then

        -- Grain of time
        g.draw(gfx.gg, 0, 0)
        g.draw(gfx.gr, 0, 0)
        g.draw(gfx.gt, 0, 0)

    elseif timer < 18.0 then

        g.draw(gfx.pl, spin * 500, 0)
        g.draw(gfx.cl, 1248 - (224 * 0.6), 32 + (224 * 0.6), 0,
            0.6, 0.6, 224, 224)
        g.draw(gfx.co, 1248 - (224 * 0.6), 32 + (224 * 0.6), spin,
            0.6, 0.6, 224, 224)

        if timer > 16 then
            g.setColor(255, 255, 255,
                math.clamp(0, timer - 16, 1) * 255)
            g.draw(gfx.en, (spin - 1) * 500, 0)
        end

    elseif timer < 18.2 then

        -- placeholder

    elseif timer < 19.0 then

        g.draw(gfx.go, 0, 0)

    elseif timer < 19.5 then

        -- placeholder

    elseif timer < 24.5 then

        local tmpTimer = (timer - 19.5) / 5

        g.draw(gfx.le, 0, 0)

        love.graphics.setScissor(40, 40 + 660 * (1 - tmpTimer),
            1200 * tmpTimer, 660 * tmpTimer)

        g.draw(gfx.lp, 0, 0)

        love.graphics.setScissor()

    elseif timer < 29 then

        -- Video

    elseif timer < 30.5 then

        g.draw(gfx.we, 0, 0)

    elseif timer < 31 then

        -- transition

    elseif timer < 32.5 then

        g.draw(gfx.dg, 0, 0)

    elseif timer < 33.5 then

        -- transition

    elseif timer < 35.0 then

        g.draw(gfx.cl, 640, 360, 1, 1, 1, 224, 224)
        g.draw(gfx.co, 640, 360, (timer - 33.5) * 3.8,
            1, 1, 224, 224)

    elseif timer < 35.8 then

        -- transition

    elseif timer < 36.8 then

        g.draw(gfx.dl, 0, 0)

    elseif timer < 37.8 then

        g.draw(gfx.de, 0, 0)

    elseif timer < 38.2 then

        -- nope

    elseif timer < 42 then

        g.draw(gfx.un, 0, 0)

    end

    g.setColor(0, 0, 0)
    g.print(timer, 10, 10)
    g.setColor(255, 255, 255)
    g.print(timer, 8, 8)

end


function math.clamp(a, b, c)

    if b < a then b = a end
    if b > c then b = c end

    return b

end
